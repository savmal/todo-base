import React, {Component} from 'react';
import { Link } from 'react-router-dom';
//import PropTypes from 'prop-types';
import axios from 'axios';
// import TodosItem from './components/todos-item.component';

const Todo = props => (
    <tr>
        <td><input type="checkbox" onChange="" /> {' '}</td>
        <td>{props.todo.title}</td>
        <td>
            <Link to={"/edit/"+props.todo._id}>Edit</Link>
        </td>
        <td>
            <button style={btnStyle} onClick={ () =>
                axios.delete(`http://localhost:4000/todos/delete/`+props.todo._id)
                    .then(() => props.updateState())
                    .catch(err => console.log(err))
            }
            >x</button>
        </td>
    </tr>
)

export default class TodosList extends Component {
    getStyle = () => {
        return {
            background: '#f4f4f4',
            hover: {
                background: '#4f4f4f'
            },
            padding: '10px',
            borderBottom: '1px #ccc dotted'
//            textDecoration: this.props.todo.completed ? 'line-through': 'none'
        }
    }

//    state = {
//        title: ''
//    }

    constructor(props) {
        super(props);

        this.onChangeTodoTitle = this.onChangeTodoTitle.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            todos: [],
            filtered: false
        };
//        this.state = {
//            title: '',
//            completed: false
//        }
    }

    fetchTodos() {
        axios.get('http://localhost:4000/todos/')
            .then(res => {
                this.setState({todos: res.data});
            })
            .catch(function (err) {
                console.log(err);
        });
    }

    fetchSelectedTodos(selection) {
//        axios.get('http://localhost:4000/todos/')
//            .then(res => {
//                this.setState({
//                    list: this.state.list.filter(function(item) {
//                        console.log(item);
//                        console.log(selection);
//                        return item.completed === selection;
//                    })
//                });
//            })
//            .catch(function (err) {
//                console.log(err);
//        });
    }

    componentDidMount() {
        this.fetchTodos();
    }

    componentDidUpdate() {
        this.fetchTodos();
    }

    todoList() {
        return this.state.todos.map(function(currentTodo, i) {
            return <Todo todo={currentTodo} key={i} />;
        });
        // { this.todoList() }
        // <td><input type="checkbox" onChange={props.markComplete.bind(props.todo._id)} /> {' '}</td>
        // <button onClick={props.delTodo.bind(props.todo._id)} style={btnStyle}>x</button>
        //<Link to={"/delete/"+props.todo._id}>Delete</Link>
        // <TodosItem todos={this.state.todos} markComplete={this.markComplete} delTodo={this.delTodo} editTodo={this.editTodo} />
    }

    todoListFilter(selection) {
//        var props = this.props;
        console.log('onclick:'+selection);
        console.log(this.state.todos);
        return this.state.todos.filter(function(currentTodo, i) {
            console.log(currentTodo.completed);
            return currentTodo.completed === selection;
        })
        .map(function(currentTodo, i) {
            return <Todo todo={currentTodo} key={i} />;
        });
    }

    getCount() {
        return this.state.todos.length === 1 ? this.state.todos.length+' item left' : this.state.todos.length+' items left';
    }

//    onSubmit = (e) => {
//        e.preventDefault();
//        this.props.addTodo(this.state.title);
//        this.setState({ title: '' });
//    }

//    onChange = (e) => this.setState({ [e.target.name]: e.target.value });

    onChangeTodoTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        console.log(`Form Submitted`);
        console.log(`Todo Title: ${this.state.title}`);
        console.log(`Todo Completed: ${this.state.completed}`);

        const newTodo = {
            title: this.state.title,
            completed: this.state.completed
        }

        axios.post('http://localhost:4000/todos/add', newTodo)
            .then(res => console.log(res.date));

        this.setState({
            title: '',
            completed: false
        });
    }

    render() {
        return (
            <div style={this.getStyle()}>
                <h3>Todos List</h3>
                <form onSubmit={this.onSubmit} style={{ display: 'flex' }}>
                    <input
                        type="text"
                        name="title"
                        className="form-control"
                        style={{ flex: '10', padding: '5px' }}
                        placeholder="What needs to be done?"
                        value={this.state.title}
                        onChange={this.onChangeTodoTitle}
                    />
                    <input
                        type="submit"
                        value="Submit"
                        className="btn btn-primary"
                        style={{ flex: '1' }}
                    />
                </form>
                <table className="table table-striped" style={{ marginTop: 20 }}>
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    { this.todoList() }
                    </tbody>
                </table>
                <table className="table table-striped" style={{ marginTop: 20 }}>
                    <tr>
                        <td>{ this.getCount() }</td>
                        <td>
                            <button style={btnSelect} onClick={ () => this.fetchTodos() }>All</button>
                        </td>
                        <td>
                            <button style={btnSelect} onClick={ () => this.todoListFilter(false) }>Active</button>
                        </td>
                        <td>
                            <button style={btnSelect} onClick={ () => this.todoListFilter(true) }>Completed</button>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

// PropTypes
//Todo.propTypes = {
//    todo: PropTypes.object.isRequired,
//    markComplete: PropTypes.func.isRequired,
//    delTodo: PropTypes.func.isRequired,
//    editTodo: PropTypes.func.isRequired
//}

const btnStyle = {
    background: '#ff0000',
    color: '#fff',
    border: 'none',
    padding: '5px 9px',
    borderRadius: '50%',
    cursor: 'pointer',
    float: 'right'
}

const btnSelect = {
    color: '#000',
    border: 'none',
    padding: '1px 1px',
    cursor: 'pointer'
}

//export default TodosList