import React, {Component} from 'react';
import axios from 'axios';

export default class EditTodo extends Component {

    constructor(props) {
        super(props);

        this.onChangeTodoTitle = this.onChangeTodoTitle.bind(this);
        this.onChangeTodoCompleted = this.onChangeTodoCompleted.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            title: '',
            completed: false
        }
    }

    componentDidMount() {
        axios.get('http://localhost:4000/todos/'+this.props.match.params.id)
            .then(res => {
                this.setState({
                    title: res.data.title,
                    completed: res.data.completed
                })
            })
            .catch(function(err) {
                console.log(err);
            })
    }

    onChangeTodoTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    onChangeTodoCompleted(e) {
        this.setState({
            completed: !this.state.completed
        })
    }

    onSubmit(e) {
        e.preventDefault();
        const obj = {
            title: this.state.title,
            completed: this.state.completed
        };

        axios.post('http://localhost:4000/todos/update/'+this.props.match.params.id, obj)
            .then(res => console.log(res.data));
        
        this.props.history.push('/');
    }

    render() {
        return (
            <div style={{marginTop: 20}}>
                <h3>Update Todo</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Title: </label>
                        <input type="text"
                               className="form-control"
                               value={this.state.title}
                               onChange={this.onChangeTodoTitle}
                               />
                    </div>
                    <div className="form-check">
                        <input type="checkbox"
                               className="form-check-input"
                               id="completedCheckbox"
                               name="completedCheckbox"
                               onChange={this.onChangeTodoCompleted}
                               checked={this.state.completed}
                               value={this.state.completed} />
                        <label className="form-check-label" htmlFor="completedCheckbox">
                            Completed
                        </label>
                    </div>
                    <br/>
                    <div className="form-group">
                        <input type="submit" value="Update Todo" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}