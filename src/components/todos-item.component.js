import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export class TodosItem extends Component {
    getStyle = () => {
        return {
            background: '#f4f4f4',
            hover: {
                background: '#4f4f4f'
            },
            padding: '10px',
            borderBottom: '1px #ccc dotted',
            textDecoration: this.props.todo.completed ? 'line-through': 'none'
        }
    }

    render() {
        const { id, title } = this.props.todo;
        return (
//            <div style={this.getStyle()} onDoubleClick={this.props.editTodo.bind(this, id, title)}>
              <div style={this.getStyle()}>
                <tr>
                   <td><input type="checkbox" onChange={this.props.markComplete.bind(this, id)} /> {' '}</td>
                   <td>{ title }</td>
                   <td>
                       <Link to={"/edit/"+id}>Edit</Link>
                   </td>
                   <td>
                       <button onClick={this.props.delTodo.bind(this, id)} style={btnStyle}>x</button>
                   </td>
               </tr>
            </div>
        )
    }
}

// PropTypes
TodosItem.propTypes = {
    todo: PropTypes.object.isRequired,
    markComplete: PropTypes.func.isRequired,
    delTodo: PropTypes.func.isRequired
}

const btnStyle = {
    background: '#ff0000',
    color: '#fff',
    border: 'none',
    padding: '5px 9px',
    borderRadius: '50%',
    cursor: 'pointer',
    float: 'right'
}

export default TodosItem
