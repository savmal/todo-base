import React, {Component} from 'react';
import axios from 'axios';

export default class CreateTodo extends Component {

    constructor(props) {
        super(props);

        this.onChangeTodoTitle = this.onChangeTodoTitle.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            title: '',
            completed: false
        }
    }

    onChangeTodoTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        console.log(`Form Submitted`);
        console.log(`Todo Title: ${this.state.title}`);
        console.log(`Todo Completed: ${this.state.completed}`);

        const newTodo = {
            title: this.state.title,
            completed: this.state.completed
        }

        axios.post('http://localhost:4000/todos/add', newTodo)
            .then(res => console.log(res.date));

        this.setState({
            title: '',
            completed: false
        });
    }

    render() {
        return (
            <div style={{marginTop: 20}}>
                <h3>Create New Todo</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Title: </label>
                        <input type="text"
                               className="form-control"
                               value={this.state.title}
                               onChange={this.onChangeTodoTitle}
                               />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Create Todo" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}