import React from 'react'

function About() {
    return (
        <React.Fragment>
            <h1>About</h1>
            <p>Todo List app v1.0.0 for Bootcamp 16 Project</p>
        </React.Fragment>
    )
}

export default About;