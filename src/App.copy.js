import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Header from './components/layout/Header';
import CreateTodo from './components/create-todo.component';
import EditTodo from './components/edit-todo.component';
import TodosList from './components/todos-list.component';
import Todos from './components/Todos';
import AddTodo from './components/AddTodo';
//import EditTodo from './components/EditTodo';
//import TodoFooter from './components/TodoFooter';
import About from './components/pages/About';
//import uuid from 'react-uuid';
import axios from 'axios';

import './App.css';
import logo from "./logo.png";

class App extends Component {
  state = {
    todos: []
    //todos: [
    //  {
    //    id: uuid(),
    //    title: 'Work on Project',
    //    completed: false
    //  },
    //  {
    //    id: uuid(),
    //    title: 'Work on Bootcamp Project',
    //    completed: false
    //  },
    //  {
    //    id: uuid(),
    //    title: 'Learn and have Fun while working',
    //    completed: false
    //  }
    //]
  }

  componentDidMount() {
//    axios.get('https://jsonplaceholder.typicode.com/todos?_limit=10')
    axios.get('http://localhost:4000/todos/')
      .then(res => this.setState({ todos: res.data }))
  }

  // Toggle Complete
  markComplete = (id) => {
    this.setState({ todos: this.state.todos.map(todo => {
      if(todo.id === id) {
        todo.completed = !todo.completed
      }
      return todo;
    }) });
  }

  // Delete Todo
  delTodo = (id) => {
//    axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
    axios.delete(`http://localhost:4000/todos/delete/${id}`)
      .then(res => this.setState({ todos: [...this.state.todos.filter(todo => todo.id !== id)] }));
    //this.setState({ todos: [...this.state.todos.filter(todo => todo.id !== id)] });
  }

  // Add Todo
  addTodo = (title) => {
//    axios.post('https://jsonplaceholder.typicode.com/todos', {
    axios.post('http://localhost:4000/todos/add', {
      title,
      completed: false
    })
      .then(res => this.setState({ todos: [...this.state.todos, res.data] }))
      .catch(error => window.alert(error));
    //const newTodo = {
    //  id: uuid(),
    //  title,
    //  completed: false
    //}
    //this.setState({ todos: [...this.state.todos, newTodo] });
  }

  // Edit Todo
  editTodo = (id, title) => {
//    axios.post(`https://jsonplaceholder.typicode.com/todos/${id}`, {
    axios.post(`http://localhost:4000/todos/update/${id}`, {
      title,
      completed: false
    })
      .then(res => this.setState({ todos: [...this.state.todos, res.data] }))
      .catch(error => window.alert(error));
  }

  render() {
    return (
      <Router>
      <div className="App">
        <div className="container">
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="/" target="_self">
              <img src={logo} width="30" height="30" alt="Todo List" />
            </a>
            <Link to="/" className="navbar-brand">Todo List</Link>
            <div className="collapse nav-collapse">
              <ul className="navbar-nav mr-auto">
                <li className="navbar-item">
                  <Link to="/" className="nav-link">Todos</Link>
                </li>
                <li className="navbar-item">
                  <Link to="/create" className="nav-link">Create Todo</Link>
                </li>
              </ul>
            </div>
          </nav>
          <Header />
          <Route path="/" exact component={TodosList} />
          <Route path="/edit/:id" component={EditTodo} />
          <Route path="/create" component={CreateTodo} />
          <Route path="/tm" render={props => (
            <React.Fragment>
              <AddTodo addTodo={this.addTodo} />
              <Todos todos={this.state.todos} markComplete={this.markComplete} delTodo={this.delTodo} editTodo={this.editTodo} />
            </React.Fragment>
          )} />
          <Route path="/about" component={About} />
        </div>
      </div>
      </Router>
    );
  }
}

export default App;
