import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Header from './components/layout/Header';
import CreateTodo from './components/create-todo.component';
import EditTodo from './components/edit-todo.component';
import TodosList from './components/todos-list.component';
// import TodosItem from './components/todos-item.component';
import Todos from './components/Todos';
import AddTodo from './components/AddTodo';
//import EditTodo from './components/EditTodo';
//import TodoFooter from './components/TodoFooter';
import About from './components/pages/About';
//import uuid from 'react-uuid';
import axios from 'axios';

import './App.css';

class App extends Component {
  state = {
    todos: []
    //todos: [
    //  {
    //    id: uuid(),
    //    title: 'Work on Project',
    //    completed: false
    //  },
    //  {
    //    id: uuid(),
    //    title: 'Work on Bootcamp Project',
    //    completed: false
    //  },
    //  {
    //    id: uuid(),
    //    title: 'Learn and have Fun while working',
    //    completed: false
    //  }
    //]
  }

  componentDidMount() {
    axios.get('http://localhost:4000/todos/')
      .then(res => this.setState({ todos: res.data }))
  }

  // Toggle Complete
  markComplete = (id) => {
    this.setState({ todos: this.state.todos.map(todo => {
      if(todo.id === id) {
        todo.completed = !todo.completed
      }
      return todo;
    }) });
  }

  // Delete Todo
  delTodo = (id) => {
    axios.delete(`http://localhost:4000/todos/delete/${id}`)
      .then(res => this.setState({ todos: [...this.state.todos.filter(todo => todo.id !== id)] }));
    //this.setState({ todos: [...this.state.todos.filter(todo => todo.id !== id)] });
  }

  // Add Todo
  addTodo = (title) => {
    axios.post('http://localhost:4000/todos/add', {
      title,
      completed: false
    })
      .then(res => this.setState({ todos: [...this.state.todos, res.data] }))
      .catch(error => window.alert(error));
    //const newTodo = {
    //  id: uuid(),
    //  title,
    //  completed: false
    //}
    //this.setState({ todos: [...this.state.todos, newTodo] });
  }

  // Edit Todo
  editTodo = (id, title) => {
    axios.post(`http://localhost:4000/todos/update/${id}`, {
      title,
      completed: false
    })
      .then(res => this.setState({ todos: [...this.state.todos, res.data] }))
      .catch(error => window.alert(error));
  }

  render() {
    return (
      <Router>
      <div className="App">
        <div className="container">
          <Header />
          <Route path="/" exact component={TodosList} />
          <Route path="/edit/:id" component={EditTodo} />
          <Route path="/create" component={CreateTodo} />
          <Route path="/old" render={props => (
            <React.Fragment>
              <AddTodo addTodo={this.addTodo} />
              <Todos todos={this.state.todos} markComplete={this.markComplete} delTodo={this.delTodo} editTodo={this.editTodo} />
            </React.Fragment>
          )} />
          <Route path="/about" component={About} />
        </div>
      </div>
      </Router>
    );
  }
}

export default App;
